
## Spotify Library Playlist

[Spotify for Developers](https://developer.spotify.com/)

## Description
In the current version of Spotify there is no way to shuffle all the songs in "Your Library". You can only shuffle "Liked Songs". This project will create a playlist of your entire library allowing you to shuffle all songs including saved tracks, created playlist, and saved albums.

## Run it
1. Follow [this tutorial](https://developer.spotify.com/web-api/tutorial/) to create your own Spotify App. Then go to `EDIT SETTIGNS` and add "http://127.0.0.1:8081/callback/" as a redirect URI.  

2. Create a file named **conf.json** in this folder with the exact same structure of the conf_example.json file.  

3. Run `python app.py`

## Project status
Beta verison complete. Basic functionality is working. V1 will work to update the UI, fix bugs, and add features. 
