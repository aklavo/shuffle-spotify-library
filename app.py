from flask import Flask, request, redirect, render_template, session
from spotify_requests import spotify
import random

app = Flask(__name__)
app.secret_key = 'some key for session'
track_uris_list = []

# ----------------------- AUTH API PROCEDURE -------------------------

@app.route("/auth")
def auth():
    return redirect(spotify.AUTH_URL)

@app.route("/callback/")
def callback():
    auth_token = request.args['code']
    auth_header = spotify.authorize(auth_token)
    session['auth_header'] = auth_header

    return profile()

def valid_token(resp):
    return resp is not None and not 'error' in resp

# -------------------------- GET LIBRARY TRACKS ----------------------------
@app.route("/get-library-tracks")
def get_library_tracks():
    if 'auth_header' in session:
        auth_header = session['auth_header']

        # --------------------- GET DATA -----------------------------------
        # Loop through get requests 50 items at a time
        def multi_get_request(request_func, id=None):
            offset = 0
            data = []
            while True:
                if id is not None:
                    d = request_func(auth_header, offset, id)
                else:
                    d = request_func(auth_header, offset)
                if len(d["items"]) == 0:
                    break
                data += d["items"]
                offset+=50
            return data
        
        # get profile data
        profile_data = spotify.get_users_profile(auth_header)
        # get user playlist data
        playlist_data = multi_get_request(spotify.get_users_playlists)
        # get user saved tracks
        saved_tracks_data = multi_get_request(spotify.get_users_saved_tracks) 
        # get user saved albums 
        saved_albums_data = multi_get_request(spotify.get_users_saved_albums) 
        
        # get tracks in user playlist
        playlist_track_data = []
        
        for playlist in playlist_data: # only pulling first 2 playlists rn cause shit break if I do all
            playlist_id = playlist['id'] 
            playlist_track_data += multi_get_request(spotify.get_playlists_items, playlist_id) 
        
        # -------------------------- ISOLATE TRACK DATA ---------------------
        library_tracks = [] # intialize list to store library tracks (track name, url, and id)
        unique_tracks = set()  # intialize set to keep track of unique tracks 
        
        # get tracks in saved albums (JSON format is slightly different than playlist and saved track data (track is not a n))
        album_track_data = []
        for album in saved_albums_data:
            album_track_data += album['album']['tracks']['items'] 
        
        for track in album_track_data:
            # pull track name, url, and id and store in dictionary
            album_track_dict = {'name': track['name'], 'url': track['external_urls']['spotify'], 'id':track['id']}
            # check if the new track is in the unique set of tracks
            if tuple(album_track_dict.items()) not in unique_tracks:
                library_tracks.append(album_track_dict) 
                unique_tracks.add(tuple(album_track_dict.items())) 
  
        library_tracks_data = saved_tracks_data + playlist_track_data
        
        for track in library_tracks_data: 
            if track['track']['id'] == None:
                continue
            #pull track name, url, and id and store in dictionary
            saved_and_playlist_track_dict = {'name': track['track']['name'], 'url': track['track']['external_urls']['spotify'], 'id':track['track']['id']}
            # check if the new track is in the unique set of tracks
            if tuple(saved_and_playlist_track_dict.items()) not in unique_tracks:
                library_tracks.append(saved_and_playlist_track_dict) # add to library track list
                unique_tracks.add(tuple(saved_and_playlist_track_dict.items())) # add to unique track set 
        
        # store track ids globally for create-playlist route
        for track in library_tracks:
            track_uris_list.append(f"spotify:track:{track['id']}")
            
        print("-"*50)
        print(len(playlist_data), "Playlists")
        print(len(saved_albums_data), "Saved Albums")
        print(len(saved_tracks_data), "Saved Tracks")
        print(len(playlist_track_data), "Tracks in all playlists")
        print(len(album_track_data), "Tracks in all saved albums")
        print("-"*50)
        
        if valid_token(profile_data):
            return render_template("profile.html",
                               user=profile_data,
                               library_tracks=library_tracks)

    return render_template('profile.html')

# --------------- CREATE LIBRARY PLAYLIST -------------------------------
@app.route("/create-playlist", methods=['POST'])
def create_playlist():
    if 'auth_header' in session:
        auth_header = session['auth_header']
        
        # get profile data
        profile_data = spotify.get_users_profile(auth_header)
        
        # get form data
        play_name = request.form['PlaylistName']
        play_descrition = request.form['PlaylistDescription']
        play_access = request.form.get('Public-Private', True)
        
        # create empty playlist based on form fields
        empty_playlist = spotify.create_playlist(auth_header, 
                                                 user_id=profile_data['id'],
                                                 name=play_name,
                                                 description=play_descrition,
                                                 access=play_access)
        
        # suffle tracks
        random.shuffle(track_uris_list)
        
        # loop through 100 songs at a time and add songs to new playlist
        for i in range(0, len(track_uris_list), 100):
            # store track uris in JSON
            track_uris_dic = {
                'uris': track_uris_list[i:i+100]
            }
            spotify.add_items_to_playlists(auth_header,
                                           uris=track_uris_dic, 
                                           playlist_id=empty_playlist['id'], 
                                           position=i)
        
        full_playlist = spotify.get_playlist(auth_header, playlist_id=empty_playlist['id'])
        
        if valid_token(profile_data):
            return render_template("profile.html",
                               user=profile_data,
                               new_playlist=full_playlist)

    return render_template('profile.html')

# --------------- RENDER HOME PAGE -------------------------------
@app.route('/')
def profile():
    if 'auth_header' in session:
        auth_header = session['auth_header']
        
        try:
            # get profile data
            profile_data = spotify.get_users_profile(auth_header)
        except Exception as e:
            print(e)
        
        if valid_token(profile_data):
            return render_template("profile.html",
                               user=profile_data)

    return render_template('profile.html')

if __name__ == "__main__":
    app.run(debug=True, port=spotify.PORT)
