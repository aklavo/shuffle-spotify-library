from __future__ import print_function
import base64
import json
import requests
import urllib.parse as urllibparse

'''
    --------------------- HOW THIS FILE IS ORGANIZED --------------------

    0. SPOTIFY BASE URL
    1. USER AUTHORIZATION
    2. USER RELATED REQUETS (NEEDS OAUTH)
    3. ALBUMS
    4. PLAYLISTS

'''

# ----------------- 0. SPOTIFY BASE URL ----------------

SPOTIFY_API_BASE_URL = 'https://api.spotify.com'
API_VERSION = "v1"
SPOTIFY_API_URL = f"{SPOTIFY_API_BASE_URL}/{API_VERSION}"

# ----------------- 1. USER AUTHORIZATION ----------------

# spotify endpoints
SPOTIFY_AUTH_BASE_URL = "https://accounts.spotify.com"
SPOTIFY_AUTH_URL = f"{SPOTIFY_AUTH_BASE_URL}/authorize"
SPOTIFY_TOKEN_URL = f"{SPOTIFY_AUTH_BASE_URL}/api/token"

# client keys
CLIENT = json.load(open('conf.json', 'r+'))
CLIENT_ID = CLIENT['id']
CLIENT_SECRET = CLIENT['secret']

# server side parameter
CLIENT_SIDE_URL = "http://127.0.0.1"
PORT = 8081
REDIRECT_URI = f"{CLIENT_SIDE_URL}:{PORT}/callback/"
SCOPE = "playlist-modify-public playlist-modify-private playlist-read-private user-library-read"
STATE = ""
SHOW_DIALOG_bool = True
SHOW_DIALOG_str = str(SHOW_DIALOG_bool).lower()

# https://developer.spotify.com/web-api/authorization-guide/
auth_query_parameters = {
    "response_type": "code",
    "redirect_uri": REDIRECT_URI,
    "scope": SCOPE,
    # "state": STATE,
    # "show_dialog": SHOW_DIALOG_str,
    "client_id": CLIENT_ID
}

URL_ARGS = "&".join(["{}={}".format(key, urllibparse.quote(val))
                for key, val in list(auth_query_parameters.items())])

AUTH_URL = f"{SPOTIFY_AUTH_URL}/?{URL_ARGS}"

'''
    This function must be used with the callback method present in the
    ../app.py file.

    This will only works if ouath == True
'''

def authorize(auth_token):

    code_query = {
        "grant_type": "authorization_code",
        "code": str(auth_token),
        "redirect_uri": REDIRECT_URI
    }
    
    base64encoded = base64.b64encode((f"{CLIENT_ID}:{CLIENT_SECRET}").encode())
    headers = {"Authorization": f"Basic {base64encoded.decode()}"}

    post_request = requests.post(SPOTIFY_TOKEN_URL, data=code_query,
                                 headers=headers)

    # tokens are returned to the app
    response_data = json.loads(post_request.text)
    access_token = response_data["access_token"]

    # use the access token to access Spotify API
    auth_header = {"Authorization": f"Bearer {access_token}"}
    return auth_header

# ------------------ 2. USER RELATED REQUESTS  ---------- 

# spotify endpoints
USER_PROFILE_ENDPOINT = f"{SPOTIFY_API_URL}/me"
USER_PLAYLISTS_ENDPOINT = f"{USER_PROFILE_ENDPOINT}/playlists"
PLAYLISTS_ENDPOINT = f"{SPOTIFY_API_URL}/playlists"
USER_TRACKS_ENDPOINT = f"{USER_PROFILE_ENDPOINT}/tracks"

# https://developer.spotify.com/web-api/get-users-profile/
def get_users_profile(auth_header):
    url = USER_PROFILE_ENDPOINT
    resp = requests.get(url, headers=auth_header)
    return resp.json()

# https://developer.spotify.com/web-api/get-a-list-of-current-users-playlists/
def get_users_playlists(auth_header,offset=0):
    query = {'limit': 50,'offset':offset}
    url = USER_PLAYLISTS_ENDPOINT
    resp = requests.get(url, params=query, headers=auth_header)
    return resp.json()

# https://developer.spotify.com/web-api/get-a-list-of-current-users-playlists/
def get_playlists_items(auth_header, offset, playlist_id):
    query = {'limit': 50,
             'offset':offset,
             'fields':'items.track(name,id,external_urls)'}
    url = f"{PLAYLISTS_ENDPOINT}/{playlist_id}/tracks"
    resp = requests.get(url, params=query, headers=auth_header)
    return resp.json()

# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-users-saved-tracks
def get_users_saved_tracks(auth_header,offset=0):
    query = {'limit': 50,'offset':offset}
    url = USER_TRACKS_ENDPOINT
    resp = requests.get(url, params=query, headers=auth_header)
    return resp.json()

# ---------------- 3. ALBUMS ------------------------
USER_ALBUM_ENDPOINT = f"{USER_PROFILE_ENDPOINT}/albums"

# https://developer.spotify.com/documentation/web-api/reference/#/operations/get-users-saved-albums
def get_users_saved_albums(auth_header,offset=0):
    query = {'limit': 50,'offset':offset}
    url = USER_ALBUM_ENDPOINT
    resp = requests.get(url, params=query, headers=auth_header)
    return resp.json()

# ----------------- 4. PLAYLISTS ----------------
# https://developer.spotify.com/documentation/web-api/reference/create-playlist
def create_playlist(auth_header,user_id, name, description, access):
    CREATE_PLAYLIST_ENDPOINT = f"{SPOTIFY_API_URL}/users/{user_id}/playlists"
    # if checkbox is checked make access False (Private)
    if access == 'on':
        access = False
    request_body = {
        'name': name,
        'description': description,
        'public': access
    }
    url = CREATE_PLAYLIST_ENDPOINT
    resp = requests.post(url, json=request_body, headers=auth_header)
    return resp.json()

#https://developer.spotify.com/documentation/web-api/reference/add-tracks-to-playlist
def add_items_to_playlists(auth_header, uris, playlist_id, position=0):
    query = {'playlist_id':playlist_id,
             'position':position}
    request_body = uris # JSON of uris {"uris": ["spotify:track:4iV5W9uYEdYUVa79Axb7Rh","spotify:track:1301WleyT98MSxVHPZCA6M", "spotify:episode:512ojhOuo1ktJprKbVcKyQ"]}
    url = f"{PLAYLISTS_ENDPOINT}/{playlist_id}/tracks"
    resp = requests.post(url, json=request_body, params=query, headers=auth_header)
    return resp.json()

#https://developer.spotify.com/documentation/web-api/reference/get-playlist
def get_playlist(auth_header, playlist_id):
    query = {'playlist_id':playlist_id,
             'fields':'name,tracks.total'} #maybe pull image if this ever works
    url = f"{PLAYLISTS_ENDPOINT}/{playlist_id}"
    resp = requests.get(url, params=query, headers=auth_header)
    return resp.json()